function addTask() {
  var taskInput = document.getElementById('taskInput');
  var taskList = document.getElementById('taskList');

  if (taskInput.value.trim() !== '') {
    var newTask = document.createElement('li');
    newTask.textContent = taskInput.value.toLowerCase();
    newTask.classList.add('mb-2', 'p-2', 'border', 'rounded', 'bg-white', 'task-transition');
    setTimeout(() => {
        newTask.classList.add('task-visible');
    }, 0);

    var deleteButton = document.createElement('button');
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.classList.add('float-right');
    deleteButton.addEventListener('click', function() {
      taskList.removeChild(newTask);
      removeTaskFromLocalStorage(newTask.textContent);
    });

    newTask.appendChild(deleteButton);
    taskList.appendChild(newTask);
    addTaskToLocalStorage(taskInput.value.toLowerCase());
    taskInput.value = '';
  }
}

function addTaskToLocalStorage(task) {
  let tasks;
  if(localStorage.getItem('tasks') === null) {
    tasks = [];
  } else {
    tasks = JSON.parse(localStorage.getItem('tasks'));
  }
  tasks.push(task);
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

function removeTaskFromLocalStorage(task) {
  let tasks;
  if(localStorage.getItem('tasks') !== null) {
    tasks = JSON.parse(localStorage.getItem('tasks'));
    tasks = tasks.filter(t => t !== task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }
}

function loadTasks() {
  let tasks;
  if(localStorage.getItem('tasks') !== null) {
    tasks = JSON.parse(localStorage.getItem('tasks'));
    tasks.forEach(task => {
      var taskList = document.getElementById('taskList');
      var newTask = document.createElement('li');
      newTask.textContent = task;
      newTask.classList.add('mb-2', 'p-2', 'border', 'rounded', 'bg-white', 'task-transition', 'task-visible');

      var deleteButton = document.createElement('button');
      deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
      deleteButton.classList.add('float-right');
      deleteButton.addEventListener('click', function() {
        taskList.removeChild(newTask);
        removeTaskFromLocalStorage(task);
      });

      newTask.appendChild(deleteButton);
      taskList.appendChild(newTask);
    });
  }
}

document.getElementById('taskInput').addEventListener('keydown', function(e) {
  if (e.key === 'Enter') {
    addTask();
  }
});

window.onload = loadTasks;
